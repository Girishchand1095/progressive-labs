// import { isTemplateElement } from '@babel/types';
import { Divider, List, ListItem, ListItemText, Typography, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import styles from './notification.module.css';
import notification from '../Notifications/notification.json';
import CloseIcon from '@material-ui/icons/Close';
import { SkeletonComp } from '../Skeleton/skeleton';


const useStyles = makeStyles((theme) => ({
    root: {
        // height:45,
        paddingTop:0,
        marginTop:0,
    },
  }));


export const Notification = (props) => {
    const classes = useStyles();
    const [loading, setLoading] = useState(true);
    const [notificationJson, setNotificationJson] = useState([]);

    const handleClose = () =>{
        props.handleClickAway();
    }

    useEffect(()=> {
        setLoading(true);

        setTimeout(() => {
            setLoading(false);
            setNotificationJson(notification);
        }, 1000)
    }, [])

    return(
        <div className={styles.notification} >
            <div className={styles.header} >
                <div className={styles.notification_header}>
                    <Typography variant="h6">
                        Notification
                    </Typography>
                </div>
                <CloseIcon onClick={handleClose} className={styles.notification_close_button} />
            </div>
            <Divider />
            <div className={styles.notification_item} >
                <List style={{padding:0}}>
                    {
                        loading ? 
                        <SkeletonComp />
                            :
                        notificationJson.map((item) => {
                            return(
                                <div className={classes.root}>
                                    <ListItem button>
                                        <ListItemText ><div className={styles.list}><Typography variant="body2">{item.title}</Typography><Typography variant="caption">{item.time}</Typography></div></ListItemText>
                                    </ListItem>
                                    <Divider />
                                </div>
                            )
                        })
                    }
                </List>  
            </div> 
        </div>
        
    )
}