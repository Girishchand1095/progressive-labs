import { Button, Grid, makeStyles, Paper, Typography } from '@material-ui/core';
// import { Height } from '@material-ui/icons';
import React from 'react';
import setup from './setup.json';
import './setup.css';

const useStyles = makeStyles((theme) => ({
    root:
    {
        height: 250,
        paddingTop: 30,
        marginLeft : 100,
        maxWidth : 480,
        paddingLeft:30

    },
    button : {
        width : 188,
        height: 50,

    }
}))



export const Setup = () => {
    const classes = useStyles();
    return(
        <div className="setup-container">
            <Paper className={classes.root} >
            <Grid container spacing={2}>
                {setup.map((item) => {
                    return(
                        <Grid item md={6} key={item.id}>
                            <div>
                                <Button variant="contained" color="primary" className={classes.button}>
                                    <Typography>
                                        {item.name } 
                                    </Typography>
                                </Button>
                                <Button variant="contained" className={classes.button}>
                                    <Typography>
                                        {item.amount}
                                    </Typography>
                                </Button>
                            </div>
                    </Grid>  
                    )
                })}
               </Grid>
               </Paper>
               </div>

            )
        }
                









//                 <Grid item md={6}>
//                         <div>
//                             <Button variant="contained" color="primary">
//                                 <Typography variant="body1">
//                                     Account Payable    
//                                 </Typography>
//                             </Button>
//                             <Button variant="contained">
//                                 <Typography>
//                                     Rs. 10,00,000
//                                 </Typography>
//                             </Button>
//                         </div>
//                 </Grid>
//                 <Grid item md={6}>
//                         <div>
//                             <div>
//                             <Button variant="contained" color="primary">
//                                 <Typography>
//                                     Account Payable    
//                                 </Typography>
//                             </Button>
//                             </div>
                            
//                             <div>
//                                 <Typography>
//                                     Rs. 10,000
//                                 </Typography>  
//                             </div>
//                         </div>
//                 </Grid>
//                 <Grid item md={6}>
//                         <div>
//                             <Button variant="contained" color="primary" className={classes.button}>
//                                 <Typography>
//                                     Account Payable    
//                                 </Typography>
//                             </Button>
//                             <Button variant="contained" className={classes.button}>
//                                 <Typography>
//                                     Rs. 10,000
//                                 </Typography>
//                             </Button>
//                         </div>
//                 </Grid>
//                 <Grid item md={6}>
//                         <div>
//                             <Button variant="contained" color="primary">
//                                 <Typography>
//                                     Account Payable    
//                                 </Typography>
//                             </Button>
//                             <Button variant="contained">
//                                 <Typography>
//                                     Rs. 10,000
//                                 </Typography>
                            
//                             </Button>
//                         </div>
//                 </Grid>

//                 <Grid item md={6}>
//                     <List>
//                         <ListItem>
//                             <Button variant="contained" color="primary">
//                                 <Typography>
//                                     Account Payable    
//                                 </Typography>
//                             </Button>
//                         </ListItem>
//                         <ListItem>
//                             <Button variant="contained">
//                                 <Typography>
//                                     Rs, 10,000   
//                                 </Typography>
//                             </Button>
//                         </ListItem>
//                     </List>
//                 </Grid>
//             </Grid>
//             </Paper>
            
           
//             <div className="account-payable">
//                 <Paper>
//                     <div className="account-payable-name">
//                         <Typography >Account Payable</Typography>
//                     </div>
                    
//                     <div className="account-payable-money">
//                         <Typography>
//                             Rs. 10,000
//                         </Typography>
//                     </div>
//                 </Paper>
                
//             </div>
         
//         </div>
//     )
// }