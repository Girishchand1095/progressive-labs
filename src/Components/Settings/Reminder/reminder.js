import { Button, Typography, withStyles } from '@material-ui/core';
import React from 'react';
import styles from'./reminder.module.css';
import AddIcon from '@material-ui/icons/Add';
import reminderInvoices from './reminderInvoices.json';
import reminderPayment from './reminderPayment.json';

const WhiteTextTypography = withStyles({
    root: {
      color: "#FFFFFF"
    }
  })(Typography);

export const Reminder = () => {
    return(
        <div className={styles.root_container} >

            {reminderInvoices.map((header) => {
                return(
                    <div key={header.id}>
                        <div className={styles.title_container} >
                            <WhiteTextTypography variant="body1">
                            {header.title}
                            </WhiteTextTypography>
                        </div>
                        <div className={styles.data_container} >
                            {header.child.map((data) => {
                                return(
                                    <div className={styles.data_container_item} >
                                        <Typography variant="body2">
                                            {data.name}
                                        </Typography>  

                                        <Typography variant="body2">
                                            {data.amount}
                                        </Typography>

                                        <Typography variant="body2">
                                            {data.date}
                                        </Typography>
                                    </div>
                                )
                            })
                            }
                        </div>
                        <div className={styles.button_container} >
                            <Button variant="contained" color="primary">
                                <AddIcon />
                                <Typography variant="body1">
                                    New Reminder
                                </Typography>
                            </Button>
                        </div>
                    </div>
                )
                }
            )} 

            {reminderPayment.map((header) => {
                return(
                    <div key={header.id}>
                        <div className={styles.title_container} >
                            <WhiteTextTypography variant="body1">
                            {header.title}
                            </WhiteTextTypography>
                        </div>
                        <div className={styles.data_container} >
                            {header.child.map((data) => {
                                return(
                                    <div className={styles.data_container_item} >
                                        <Typography variant="body2">
                                            {data.name}
                                        </Typography>  

                                        <Typography variant="body2">
                                            {data.amount}
                                        </Typography>

                                        <Typography variant="body2">
                                            {data.date}
                                        </Typography>
                                    </div>
                                )
                            })
                            }
                        </div>
                   
                   
                        <div className={styles.button_container} >
                            <Button variant="contained" color="primary">
                                <AddIcon />
                                <Typography variant="body1">
                                    New Reminder
                                </Typography>
                            </Button>
                        </div>
                    </div>
               
                )
                }
            )}

        </div>
    )
}


