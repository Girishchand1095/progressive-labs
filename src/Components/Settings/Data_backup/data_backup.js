import { Typography } from '@material-ui/core';
import React from 'react';
import Button from '@material-ui/core/Button';
import styles from './data_backup.module.css';

export const Databackup = () => {
    return(
        <div className={styles.data_backup_section} >
            <div className={styles.paragraph} >
                <div className={styles.first_line} >
                    <Typography variant="subtitle2" style={{fontWeight: "bold"}}>
                        Export all your data such as Estimates, Invoices, Credit Notes etc from PES on to a CSV file. You will receive 
                    </Typography>
                </div>
                <div className={styles.second_line} >
                    <Typography variant="subtitle2" style={{fontWeight: "bold"}} >
                        an email with a link to download your data a few minutes after you click on Backup Your Data.
                    </Typography>
                </div>
            </div>
              

            <div className={styles.data_backup_btn} >
                <Button variant="contained" color="primary">
                    Data Backup
                </Button>
            </div>
            
            
        </div>
    )
}