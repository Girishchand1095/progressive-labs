import { Divider, List, ListItem, ListItemText, Typography, makeStyles } from '@material-ui/core';
import React, { useEffect, useState } from 'react';
import setting from "../Settings/setting.json";
import styles from './setting.module.css';
import {
    Link
  } from "react-router-dom";
import CloseIcon from '@material-ui/icons/Close';
import { SkeletonComp } from '../Skeleton/skeleton';

  const useStyles = makeStyles((theme) => ({
    root: {
    // height:45,

    },
  }));


export const Setting = (props) => {
    const classes = useStyles();
    const [settingJson, setsettingJson] = useState([]);
    const [loading, setLoading] = useState(true);

    const handleClose = () => {
        props.handleClickAway();
    }

    useEffect(() => {
        setLoading(true);
        setTimeout(() => {
            setLoading(false);
            setsettingJson(setting);
        }, 2000)
    }, [])

    return(
        <div className={styles.setting} >

            
            <div className={styles.setting_header}>
                <div className={styles.setting_header_setting}>
                    <Typography variant="h6" gutterBottom>
                        Setting
                    </Typography>
                </div>
                <CloseIcon onClick={ handleClose } className={styles.setting_close_button} />
            </div>
            <Divider />
            <div className={styles.setting_item}>
            <List style={{padding:0}}>
            
                {loading ? 
                    <SkeletonComp />
                        :
                    settingJson.map((item) => {
                        return(
                            <div key={item.id} className={classes.root}>
                                <ListItem button>
                                    <ListItemText>
                                        <Link to = {item.path} style={{ textDecoration: 'none'}} onClick={ handleClose }>
                                            <Typography variant="body1">
                                                {item.name}
                                            </Typography>
                                        </Link>
                                    </ListItemText>
                                </ListItem>
                                <Divider />                              
                            </div>
                        )
                    })
                }
            {}
        </List>
        </div>
        </div>
    )
}