import React, { useEffect, useState } from 'react';
import profile_data from "../Profile/profile_data.json";
import account_data from "../Profile/account_data.json";
import styles from './profile.module.css';
import { Avatar, Divider, List, ListItem, ListItemText, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core';
import { Link } from 'react-router-dom';
import CloseIcon from '@material-ui/icons/Close';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import SupervisorAccountIcon from '@material-ui/icons/SupervisorAccount';
import { SkeletonComp } from '../Skeleton/skeleton';
import { SkeletonProfile } from '../Skeleton/SkeletonProfile/skeletonprofile';
import { SkeletonProfileData } from '../Skeleton/SkeletonProfile/skeletonprofile-data';


const useStyles = makeStyles((theme) => ({
    root: {
        listStyle:null,
    }
  }));


export const Profile = (props) => {
    const classes = useStyles();

    const [account_dataJson, setAccount_dataJson] = useState([]);
    const [profile_dataJson, setProfile_dataJson] = useState([]);
    const [loading, setLoading] = useState(true);
    const [avatar, setAvatar] = useState(true);
    const handleClose =() => {
        props.handleClickAway();
    }

    useEffect(() => {
        setLoading(true);
        setAvatar(true);
        setTimeout(() => {
            setLoading(false);
            setAvatar(false);
            setAccount_dataJson(account_data);
            setProfile_dataJson(profile_data);
        }, 2000)
  }, [])

    return(
        <div className={styles.profile} >
            <div className={styles.profile_data} >
                <CloseIcon onClick={handleClose} className={styles.profile_close_button} />
                {avatar ? 
                <SkeletonProfile />
                    :
                <Avatar src="/broken-image.jpg" className= {classes.large}/>
                }
                
                {loading ? 
                <SkeletonProfileData />
                
                :
        
                profile_dataJson.map((item) => {
                    return(
                        <div>
                            <Typography variant="caption">{item.name}</Typography>
                        </div>
                    )
                })

            }
            </div>
            <Divider />        

        <div className={styles.account_data} >
            <List style={{padding:0}}>
                {loading ? 
                    <SkeletonComp />
                        :
                        account_dataJson.map((item) => {
                            return(
                                <div className={classes.root}>
                                    
                                        <ListItem button>
                                            <ListItemText>
                                                <Link to = {item.path} style={{ textDecoration: 'none' }} onClick={handleClose}>
                                                    
                                                    <div className={styles.account_data_item} >
                                                        <SupervisorAccountIcon />
                                                        <Typography variant="body1">
                                                            {item.name}
                                                        </Typography>
                                                    </div>
                                                </Link>
                                            </ListItemText>
                                        </ListItem>
                                        <Divider />
                                </div>
                            )
                        })
                }
                
                </List> 
            </div>
            <div className={styles.footer} >
                <div>
                    <ExitToAppIcon />
                </div>
                <div>
                    <Typography>
                        Log Out
                    </Typography>
                </div>
            </div>
           
        </div>
    )
}