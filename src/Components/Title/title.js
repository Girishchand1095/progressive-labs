import React from 'react';
import { Typography } from '@material-ui/core';
// import { Link } from 'react-router-dom';
import styles from './title.module.css';

import properties from './../properties/properties.json';
import { Property } from '../properties/properties';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
    root: {
      color: '#072359'
    }
  });

export const Title = () => {
    const classes = useStyles();
    
    return(
        <div className={styles.container}>
          

           
            {properties.map((item) =>{
                    return(
                       
                        <div key={item.id} className={styles.grid_container}>
                        <Typography variant="h6" className={classes.root}> { item.name } </Typography>
                            <Property child={item.child} />
                        </div>                    
                    )
                })
                
                }
        </div>
        
     
      
    )
}