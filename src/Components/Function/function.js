const ArrOfVariable = [
  {
    "id" : "1",
    "category" : "fruit",
    "item" : "orange",
    "price" : 10
  },
  {
    "id" : "2",
    "category" : "vegetables",
    "item" : "spinach",
    "price" : 20
  },
  {
    "id" : "3",
    "category" : "fruit",
    "item" : "mango",
    "price" : 40
  },
  {
    "id" : "4",
    "category" : "clothes",
    "item" : "t-shirt",
    "price" : 100
  },
  {
    "id" : "5",
    "category" : "fruit",
    "item" : "apple",
    "price" : 150
  },
  {
    "id" : "6",
    "category" : "vegetables",
    "item" : "onion",
    "price" : 60
  },
  {
    "id" : "7",
    "category" : "clothes",
    "item" : "cap",
    "price" : 50
  },
  {
    "id" : "8",
    "category" : "fruit",
    "item" : "banana",
    "price" : 20
  }
]