const ArrOfVariable = [
    {
        "id": "1",
        "category": "fruit",
        "item": "orange",
        "price": 10
    },
    {
        "id": "2",
        "category": "vegetables",
        "item": "spinach",
        "price": 20
    },
    {
        "id": "3",
        "category": "fruit",
        "item": "mango",
        "price": 40
    },
    {
        "id": "4",
        "category": "clothes",
        "item": "t-shirt",
        "price": 100
    },
    {
        "id": "5",
        "category": "fruit",
        "item": "apple",
        "price": 150
    },
    {
        "id": "6",
        "category": "vegetables",
        "item": "onion",
        "price": 60
    },
    {
        "id": "7",
        "category": "clothes",
        "item": "cap",
        "price": 50
    },
    {
        "id": "8",
        "category": "fruit",
        "item": "banana",
        "price": 20
    }
]


//create one arrow function
// const arrayManipulation = () => {
//     console.log(ArrOfVariable.map((item, index) => {
//             return item.price + 10;
//         })
//     )


//     console.log(ArrOfVariable.forEach((item, index) => {
//         })
//     )
//
// }

// const output = ArrOfVariable.map((items, index) => {
    // if(items.price >= 0){
    // const newArray =
    //     {
    //         "id": items.id,
    //         "category": items.category,
    //         "item": items.item,
    //         "price": items.price + 10
    //     }
    // }

//     return newArray;
// })

// console.log(output);

//only fruits = [orange, mango]

//
// const filteredFruits = ArrOfVariable.map((items, index) => {
//     if(items.category === "fruit") {
//         return items.item;
//     }
// })

// console.log(filteredFruits);


//using foreach
//  console.log(ArrOfVariable.forEach((items, index) => {
//      if(items.category === "fruit") {
//          return items.item;
//      }
//  }))
// console.log(catFruits);

// function catFruits (i) {
//      if(i.category === "fruit") {
//          return i.item;
//      }
// }
// const Fruits = ArrOfVariable.forEach(catFruits);
//
// let tempString = '';
//
// ArrOfVariable.forEach((item, index)=> {
//     if(item.category === 'fruit'){
//         tempString += item.item + ','
//     }
// })
// console.log(tempString);


// arrayManipulation();


//Chaining map and filter

// const arr = ArrOfVariable.filter((item, index ) => item.category === 'fruit').map((items, index) => {
//     return items.item;
// }).join(',')

// console.log(arr);

const arr = ArrOfVariable.reduce((acc, curr) => {

    if(curr.category === 'fruit') {
        acc.push(curr.item)
    }
    return acc

}, []).join(',');
console.log(arr);



const sum = ArrOfVariable.reduce(function (acc, cur){
    acc += cur.price;
    return acc;
}, 0)
// console.log("sum =",sum);

//alternative way chaining

const total = ArrOfVariable.filter((i) => i.price ).reduce((acc, cur) => {
    acc = acc + cur.price;
    return acc;
}, 0)
// console.log(total);



//functional perspective to find area of circle
const radius = [1, 2, 3]

const calculateArea = function (){
    const output = [];

    for(let i=0; i<radius.length; i++){
        output.push(3.14 * radius[i] * radius[i]);
    }
    return output;
}

// console.log(calculateArea(radius));