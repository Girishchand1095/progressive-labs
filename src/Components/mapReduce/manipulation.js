import React, {useEffect, useState} from "react";
import Button from '@material-ui/core/Button';
import styles from './manipulation.module.css';
import {ListView} from "./listView/listView";

const axios = require('axios');

export const Manipulation = () => {

    const [responseData, setResponseData] = useState([]);

    const handleRemove = (id) => {
        setResponseData(responseData.filter((item) => item.id !== id));
    }

    const getTitle = () => {
        axios.get("https://jsonplaceholder.typicode.com/todos")
            .then(function (response) {
                // console.log(response.data);
                setResponseData(response.data);
            })
            .catch(function (error) {
                console.log(error);
            })
    }
    // useEffect(() => {
    //     getTitle();
    // }, [])
    useEffect(() => {
        getTitle();
    }, [])



    return (
        <>
            <div>
                <div className={styles.manipulation}>
                    {responseData.map((item, index) => {
                        return <ListView item={item} handleRemove={handleRemove} index={index}/>
                    })}
                </div>
            </div>
        </>
    )
}