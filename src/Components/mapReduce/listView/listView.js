import React, {useState} from "react";
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Divider from '@material-ui/core/Divider';
import Grid from '@material-ui/core/Grid';
import styles from './listView.module.css';
import {makeStyles} from '@material-ui/core/styles';

import DeleteIcon from '@material-ui/icons/Delete';
import axios from "axios";

const useStyles = makeStyles({
    table: {
        minWidth: 900,
        width: 900,
    },
    delete: {

    }
});

export const ListView = (props) => {
    const classes = useStyles();

    const [resp, setResp] = useState(props.item);



    return (
        <>
            <div className={styles.listView}>
                <Grid>
                    <ListItem>
                        <Grid item xs={2}>
                            {props.index + 1}
                        </Grid>
                        <Grid item xs={9}>
                            {props.item.title}
                        </Grid>
                        <Grid item xs={1}>
                            <DeleteIcon onClick={() => props.handleRemove(props.item.id)} className={classes.delete}/>
                        </Grid>

                    </ListItem>
                    <Divider/>
                </Grid>
            </div>
        </>
    )
}
