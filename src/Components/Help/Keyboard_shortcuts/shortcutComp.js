import React from "react";
import { Typography, withStyles } from "@material-ui/core";
import shortcuts from './shortcuts.json';
// import AddIcon from '@mui/icons-material/Add';
import './shortcutComp.css';

const WhiteTextTypography = withStyles({
    root: {
      color: "#FFFFFF"
    }
  })(Typography);

  const BlueTextTypography = withStyles({
    root: {
      color: "#072359",
      fontWeight: "bold"
    }
})(Typography);

export const ShortcutComp = () => {
    return(
        <div className="body-items">
            <div className="button-group-alt-shift">
                <div >
                    <button className="outerShift">
                        <div className="buttonShift">
                            {/* <Typography variant="body1">
                                Shift
                            </Typography> */}
                            <WhiteTextTypography variant="body1">
                                Shift
                            </WhiteTextTypography>
                        </div>
                                              
                    </button>
                </div>
                <div className="button-add">
                    <Typography variant="h6" gutterBottom>
                        +
                    </Typography>                
                </div>  
                <div>
                    <button className="outerAlt">
                        <div className="buttonAlt">
                            <WhiteTextTypography variant="body1">
                                Alt
                            </WhiteTextTypography> 
                        </div>
                           
                    </button>
                </div>                 
            </div>
        
            <div className="mid-buttonAdd">
                <Typography variant="h6">
                    +
                </Typography>
                
            </div>
        
            <div>
                {
                    shortcuts.map((item) => {
                        return(
                            <div className="button-property">
                                <button className="buttons">
                                    <div className="buttonKey"> 
                                        <WhiteTextTypography variant="body1">
                                            {item.button}
                                        </WhiteTextTypography>
                                    </div>
                                </button>
                                
                                <BlueTextTypography variant="body1">
                                    {item.name}
                                </BlueTextTypography>
                            </div>
                        )
                        
                    })
                }        
            </div>
        </div>
    )
}