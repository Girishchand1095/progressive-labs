import React from "react";
import { Typography, withStyles } from "@material-ui/core";
import CloseIcon from '@material-ui/icons/Close';
import { ShortcutComp } from "./shortcutComp";
import styles from './shortcuts.module.css';


const BlueTextTypography = withStyles({
    root: {
      color: "#072359",
      fontWeight : "bold"
    }
})(Typography);

export const Keyboards = () => {
    // const handleClose = () => {
    //     Props.handleClickAway();
    //    };

    return(
        <div className = {styles.main_body} >
            <div className={styles.body_title}>
                <div className={styles.main_title}>
                    <BlueTextTypography variant="body1">
                        Keyboards Shortcuts
                    </BlueTextTypography>
                </div>
                {/* <CloseIcon onClick= { handleClose}/>    */}
                <CloseIcon className={styles.closeButton} style={{height : 30, width:30}} />
            </div>
            <div className={styles.lower_body}>
                <div className={styles.lower_body_item1}>
                    <ShortcutComp />
                    <ShortcutComp />
                    <ShortcutComp />
                </div>
                <div className={styles.lower_body_item2}>
                    <ShortcutComp />
                    <ShortcutComp />
                    <ShortcutComp />
                </div>

            </div>
           
           
        </div>
    )
}

