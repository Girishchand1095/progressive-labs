import React, { useState } from "react";
import { Button, Grid, Typography } from "@material-ui/core";
import styles from './user_manual.module.css';
// import YouTube from 'react-youtube';
import ReactPlayer from 'react-player';


export const User_manual = () => {

    const [state, setState] = useState({
        playing: false
    })
    const {playing} = state;

    const handlePlayPause = () => {
        setState({...state, playing : !state.playing});
    }

    return(
        <>
            <div className={styles.user_manual_main} >
                <Grid container justifyContent={"center"}>
                    <Grid item >
                        <p>
                            <Typography>
                                How to use PES?
                            </Typography>
                        </p>
                    </Grid>
                </Grid>

                <Grid style={{height : 400}}>
                    <Grid item style={{height : 400}}>
                        <div className={styles.playerWrapper} >
                            <ReactPlayer
                                className={styles.reactPlayer}
                                url='https://www.youtube.com/watch?v=ysz5S6PUM-U'
                                playing = {playing}
                                width='70%'
                                height='60%'
                                controls={true}
                            />
                        </div>
                    </Grid>
                </Grid>

                <Grid container justifyContent={"center"}>
                    <Grid item>
                        <div className={styles.user_manual_button} >
                            <Button variant="contained" color="primary" onClick={handlePlayPause}>
                                <Typography>
                                    Download as PDF
                                </Typography>
                            </Button>
                        </div>
                    </Grid>
                </Grid>
            </div>
        </>
    )
}

