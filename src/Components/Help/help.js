import { Divider, List, ListItem, ListItemText, Typography, makeStyles } from '@material-ui/core';
import Button from '@material-ui/core/Button';
import React, { useEffect, useState } from 'react';
import help from '../Help/help.json';
import styles from './help.module.css';
import {
    Link
  } from "react-router-dom";
  import CloseIcon from '@material-ui/icons/Close';
import { SkeletonComp } from '../Skeleton/skeleton';

  const useStyles = makeStyles((theme) => ({
    root: {
        // height:45,
    },
  }));

export const Help = (Props) => {
const classes = useStyles();
const [loading, setLoading] = useState(true);
const [helpJson, setHelpJson] = useState([]);


  const handleClose = () => {
   Props.handleClickAway();
  };

    useEffect(() => {
        setLoading(true);

        setTimeout(() => {  
            setLoading(false);
            setHelpJson(help);
        }, 2000)
        
    }, [])

    return(
        <div className={styles.help_section}>
                
                <div className={styles.header_help_section} >
                    <div className={styles.header_help} >
                        <Typography variant="h6">
                            Help
                        </Typography>
                    </div>
                    <CloseIcon onClick= { handleClose} className={styles.close_button_help} />            
                </div>
                <Divider />


                <div className={styles.upper_section} >
                    <List style={{padding: 0}}>
                    {
                    loading ? 
                        // <Skeleton />
                        <SkeletonComp />
                     : 
                    helpJson.map((item) => {
                        return(
                            <div className={classes.root}>  
                                <ListItem button>
                                    <ListItemText>
                                        <Link to = {item.path} style={{ textDecoration: 'none' }} onClick= { handleClose}>
                                            <Typography variant="body1" >
                                                { item.name }
                                            </Typography>
                                        </Link>        
                                    </ListItemText>                                 
                                </ListItem>
                                <Divider />
                            </div>  
                        )
                    })
                    }

                        
                    </List>       
                </div>

            <div className={styles.lower_container} >
                <Typography variant="body1"><p>Need Assistance ?</p></Typography>
                <Typography variant="caption"><p>Chat With Us</p></Typography>
                <Button size="medium" variant="contained" color = "primary">
                    Chat Now
                </Button>
            </div>
        </div>  
    )
}
