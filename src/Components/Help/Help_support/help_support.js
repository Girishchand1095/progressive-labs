import React from 'react';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import { Divider, Typography } from '@material-ui/core';
import help_support from './help_support.json';
import styles from './help_support.module.css';

export const Helpsupport = () => {
    return(
        <div className={styles.help_support_container} >
                <div className={styles.nav_button} >
                    <div>
                        <Button variant="contained" color="primary">
                            <AddIcon />
                            <Typography variant="body1">
                                Create New Ticket
                            </Typography>
                        </Button>
                    </div>

                    <div>
                        <Button variant="contained" color="primary">
                            <AddIcon />
                            <Typography variant="body1">
                                Email
                            </Typography>
                        </Button>
                    </div>
                    
                    <div>
                        <Button variant="contained" color="primary">
                            <AddIcon />
                            <Typography variant="body1">
                                Call
                            </Typography>
                        </Button>
                    </div>      
                </div>

                <div className={styles.main_section_body} >
                    {
                     help_support.map((item)=> {

                        let pending = item.status === 'Pending' ? styles.pendingTickets 
                        : styles.successTickets;

                         return(
                             <div className={ `${styles.main_section_body_item} ${pending} `}>
                                 <div className={styles.main_section_body_item_title} >
                                    <Typography variant="h6">
                                        {item.title}
                                    </Typography>
                                    <Typography variant="h6">
                                        {item.date}
                                    </Typography>
                                 </div>
                                <Divider />
                                <div className={styles.main_section_body_item_body} > 
                                    <Typography variant="body1">
                                        {item.serial_number}
                                    </Typography>
                                    <Typography variant="body1">
                                        {item.status}
                                    </Typography>
                                </div>                           
                             </div>
                         )
                    })}
                </div>
        </div>
    )
}