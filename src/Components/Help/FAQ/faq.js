import React from 'react';
import styles from './faq.module.css';
import faq from './faq.json';
import { FaqComp } from './faqComp';


export const FAQ = () => {
    
    return(
        <div className = {styles.faq_section} >           
            {faq.map((item) => {
                return(
                    <div key={item.id}>
                        <FaqComp item={item}/> 
                    </div>
                    
                )
            })
            }           
        </div>
    )
}