import React from "react";
import { Collapse, Typography, withStyles } from "@material-ui/core";
import styles from './faqComp.module.css';

const WhiteTextTypography = withStyles({
    root: {
      color: "#FFFFFF",
      paddingLeft : 20,
      paddingTop : 10
    }
})(Typography);

const SignTextTypography = withStyles({
    root: {
      color: "#FFFFFF"
    }
})(Typography);

export const FaqComp = (Props) => {

    const [checked, setChecked] = React.useState(false);

    const handleClick = () => {
      setChecked((prev) => !prev);
    };
    return(
        <>
        <div className={styles.faq_description} checked={checked} onClick={handleClick} style={{zIndex:1}}>
                                <div>
                                    <WhiteTextTypography gutterBottom>
                                        {Props.item.description}
                                    </WhiteTextTypography>
                                </div>
                                <div>
                                    <SignTextTypography variant="h4" gutterBottom>
                                        {Props.item.sign}
                                    </SignTextTypography>
                                </div>
                            
                        </div>
                            <Collapse in={checked} >
                                <div className={styles.faq_description_paragraph} style={{marginTop: 0}}>
                                    <div>
                                        <Typography variant="subtitle2">
                                        {Props.item.answer}
                                        </Typography>
                                    </div>
                                </div>
                            </Collapse>
        </>
     
    )
}