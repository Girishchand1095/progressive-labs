import React from 'react';
import { Typography } from '@material-ui/core';
import AddCircleOutlineIcon from '@material-ui/icons/AddCircleOutline';
import {
    Link
  } from "react-router-dom";
import styles from './properties.module.css';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
  root: {
    color : '#636363',  
  }
});



export const Property = (props) => {
    const classes = useStyles();
    
    return(
        <div >
            <ul>
                {props.child.map((sub)=> {
                    return (
                        <Link to={sub.path} style={{textDecoration: 'none'}}> 
                            <li>                                                    
                                <AddCircleOutlineIcon style={{ fontSize: 20 }} />   
                                <Typography variant="body1" gutterBottom> 
                                    {sub.name}
                                </Typography>                         
                            </li>
                        </Link>
                    )
                }                    
                )}
            </ul>                   
        </div>  
    )
}