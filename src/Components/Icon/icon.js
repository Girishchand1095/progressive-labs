import React, { useState } from 'react';
import {ClickAwayListener, IconButton, Tooltip, Typography } from '@material-ui/core';
import Popper from '@material-ui/core/Popper';
import { Title } from '../Title/title';
import HelpIcon from '@material-ui/icons/Help';
import NotificationsNoneIcon from '@material-ui/icons/NotificationsNone';
import SettingsIcon from '@material-ui/icons/Settings';
import { Avatar } from '@material-ui/core';
import { Help } from './../Help/help';
import { Notification } from '../Notifications/notification';
import { Setting } from '../Settings/setting';
import { Profile } from '../Profile/profile';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import styles from './icon.module.css';


export const Icon = () =>{

    const [anchorEl, setAnchorEl] = React.useState(false);
    const [currentShowing, setCurrentShowing] = useState(null);

    const handleClick = (name) => (event) => {

        // console.log(name, currentShowing);

        if(name === currentShowing){
            setCurrentShowing(null)
        }else{
            setCurrentShowing(name);
        } 

        setAnchorEl(name === currentShowing ? null:   event.currentTarget);
       
    };
   
    const handleClickAway = () => {
        handlePopperClose(); 
    };

    const handlePopperClose = () => {
        setCurrentShowing(null);
        setAnchorEl(false);  
    }   

    const open = Boolean(anchorEl);
    const id = open ? 'simple-popper' : undefined;


    return(
        <ClickAwayListener onClickAway={handleClickAway}>
            <div className={styles.icon} >   
                <div>
                    <Tooltip title="Add New" aria-label="add" arrow>
                        <Button variant="contained" color="primary" aria-describedby={id} onClick={handleClick('navigation')}>
                            <AddIcon /> Add New
                        </Button>
                    </Tooltip>
                </div>
                
                <div className={styles.right_nav} >
                    <div >
                        <Tooltip title="Help" aria-label="help" arrow>
                            <div onClick={handleClick('help')} className={styles.help_icon}>
                                <IconButton>
                                    <HelpIcon  /><Typography variant="body2">Help</Typography>
                                </IconButton>
                                    
                            </div>
                        </Tooltip> 
                        
                        <Popper id={id} open={currentShowing === 'help'} anchorEl={anchorEl} keepMounted={false}>
                        <Help handleClickAway={handlePopperClose}/>
                        </Popper> 
                         
                    </div>
                                
                    <Tooltip title="Notifications" aria-label="notification" arrow>
                        <IconButton>
                            <NotificationsNoneIcon onClick={handleClick('notification')} /> 
                        </IconButton>
                        
                    </Tooltip>
                    <Popper id={id} open={currentShowing === 'notification'} anchorEl={anchorEl} keepMounted={false}>
                        <Notification handleClickAway={handlePopperClose}/>
                    </Popper>

                    <Tooltip title="Settings" aria-label="settings" arrow>
                        <IconButton>
                            <SettingsIcon onClick={handleClick('setting')} />
                        </IconButton>                      
                    </Tooltip>
                    <Popper id={id} open={currentShowing === 'setting'} anchorEl={anchorEl} keepMounted={false}>
                        <div><Setting handleClickAway={handlePopperClose}/></div>
                    </Popper>
                    
                    <Tooltip title="Username" aria-label="avatar" arrow>
                        <IconButton>
                            <Avatar src="/broken-image.jpg" onClick={handleClick('avatar')}/>
                        </IconButton>  
                    </Tooltip>
                    <Popper id={id} open={currentShowing === 'avatar'} anchorEl={anchorEl} keepMounted={false}>
                        <Profile handleClickAway={handlePopperClose} />
                    </Popper>
                </div>
                
                <Popper id={id} open={currentShowing === 'navigation'} anchorEl={anchorEl} keepMounted={false}>
                    <div> <Title handleClickAway={handlePopperClose}/></div>
                </Popper>

            </div>   
           
        </ClickAwayListener> 
    
    )
      
}
