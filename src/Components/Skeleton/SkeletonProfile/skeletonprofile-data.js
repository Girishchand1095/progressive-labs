import React from 'react';
import Skeleton from '@material-ui/lab/Skeleton';

import './skeletonprofile-data.css';

export const SkeletonProfileData = () => {
    return(
        <div className="skeletonprofile-data">
            <Skeleton />
            <Skeleton animation="wave" />
            <Skeleton animation="wave" />
        </div>
    )
}