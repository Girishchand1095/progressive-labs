import React from 'react';
// import { SkeletonComp } from '../skeleton';
import { Skeleton } from '@material-ui/lab';
import './skeletonprofile.css';

export const SkeletonProfile = () => {
    return(
        <div>
            <div className="skeleton-avatar">
                <Skeleton variant="circle" width={60} height={60} animation="wave"  />
            </div>
            
        </div>
    )
}