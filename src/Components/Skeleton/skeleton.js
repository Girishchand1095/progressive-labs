import React from 'react';
import './skeleton.css';
import Skeleton from '@material-ui/lab/Skeleton';

export const SkeletonComp = () => {
    return(
        <div className="skeleton">
            <Skeleton height={50}/>
            <Skeleton height={50} />
            <Skeleton animation="wave" height={50} />
        </div>
    )
}

