import './App.css';
import { Icon } from './Components/Icon/icon';
import {
  BrowserRouter as Router,
  Route
} from "react-router-dom";
import { Shareholders } from './Components/Navigation_items/Company/Shareholders/shareholder';
import { Branches } from './Components/Navigation_items/Company/Branches/branches';
import { Employee } from './Components/Navigation_items/Company/Employee/employee';
import { Userroles } from './Components/Navigation_items/Company/User_roles/user_roles';
import { Banks } from './Components/Navigation_items/Company/Banks/banks';
import { Vendors } from './Components/Navigation_items/Company/Vendors/vendors';
import { Accountgroup } from './Components/Navigation_items/Accounting_setups/Account_group/account_group';
import { Accounthead } from './Components/Navigation_items/Accounting_setups/Account_head/account_head';
import { Journalvoucher } from './Components/Navigation_items/Voucher_entries/Journal_voucher/journal_voucher';
import { Receiptvoucher } from './Components/Navigation_items/Voucher_entries/Receipt_voucher/receipt_voucher';
import { Paymentvoucher } from './Components/Navigation_items/Voucher_entries/Payment_voucher/payment_voucher';
import { Requestforproposal } from './Components/Navigation_items/Purchase/Request_for_proposal/request_for_proposal';
import { Purchaseorder } from './Components/Navigation_items/Purchase/Purchase_order/purchase_order';
import { Purchasebillentry } from './Components/Navigation_items/Purchase/Purchase_bill_entry/purchase_bill_entry';
import { Purchasereturn } from './Components/Navigation_items/Purchase/Purchase_return/purchase_return';
import { Inventorylevels } from './Components/Navigation_items/Inventory/Inventory_levels/inventory_levels';
import { Inventoryadjustments } from './Components/Navigation_items/Inventory/Inventory_adjustments/inventroy_adjustments';
import { Customer } from './Components/Navigation_items/Sales/Customer/customer';
import { Proformaestimate} from './Components/Navigation_items/Sales/Proforma_estimate/proforma_estimate';
import { Invoices } from './Components/Navigation_items/Sales/Invoices/invoices';
import { Returncreditnotes } from './Components/Navigation_items/Sales/Return_credit_notes/return_credit_notes';
import { Generateledger } from './Components/Navigation_items/Reports/Generate_ledger/generate_ledger';
import {Groupsummary } from './Components/Navigation_items/Reports/Group_summary/group_summary';
import { Profitloss } from './Components/Navigation_items/Reports/Profit_loss/profit_loss';
import { Trialbalance } from './Components/Navigation_items/Reports/Trial_balance/trial_balance';
import { Balancesheet } from './Components/Navigation_items/Reports/Balance_sheet/balance_sheet';
import { Helpsupport } from './Components/Help/Help_support/help_support';
import { FAQ } from './Components/Help/FAQ/faq';
import { Datamigration } from './Components/Help/Data_migration/data_migration';
import { Organizationalprofile } from './Components/Settings/Organizational_profile/organizational_profile';
import { Userrole } from './Components/Settings/User_role/user_role';
import { Openingbalance } from './Components/Settings/Opening_balance/opening_balance';
import { Preference } from './Components/Settings/Preference/preference';
import { Reminder } from './Components/Settings/Reminder/reminder';
import { Databackup } from './Components/Settings/Data_backup/data_backup';
import { Accountsetting } from './Components/Profile/Account_setting/account_setting';
import { Changepassword } from './Components/Profile/Change_password/change_password';
import { Keyboards } from './Components/Help/Keyboard_shortcuts/shortcuts';
import { User_manual } from './Components/Help/User_manual/user_manual';
import {Manipulation} from "./Components/mapReduce/manipulation";

function App() {
  return (
    <div className="root_container">
    
      <Router>
    
        <Icon />
        {/* <Setup /> */}

      {/* //Add Button routes */}
        {/* <Route path='/shareholders' exact> */}

        <switch></switch>
        <Route exact path='/shareholders'>
          <Shareholders />
        </Route> 
        <Route path='/branches'>
          <Branches />
        </Route>
        <Route path='/employee'>
          <Employee />
        </Route>
        <Route path='/user_roles'>
          <Userroles />
        </Route>
        <Route path='/banks'>
          <Banks />
        </Route>
        <Route path='/vendors'>
          <Vendors />
        </Route>

        <Route path='/account_group'>
          <Accountgroup />
        </Route>
        <Route path='/account_head'>
          <Accounthead />
        </Route>

        <Route path='/journal_voucher'>
          <Journalvoucher />
        </Route>
        <Route path='/receipt_voucher'>
          <Receiptvoucher />
        </Route>
        <Route path='/payment_voucher'>
          <Paymentvoucher />
        </Route>

        <Route path='/request_for_proposal'>
          <Requestforproposal />
        </Route>
        <Route path='/purchase_order'>
          <Purchaseorder />
        </Route>
        <Route path='/purchase_bill_entry'>
          <Purchasebillentry />
        </Route>
        <Route path='/purchase_return'>
          <Purchasereturn />
        </Route>

        <Route path='/inventory_levels'>
          <Inventorylevels />
        </Route>
        <Route path='/inventory_adjustments'>
          <Inventoryadjustments />
        </Route>

        <Route path='/customer'>
          <Customer />
        </Route>
        <Route path='/proforma_estimate'>
          <Proformaestimate />
        </Route>
        <Route path='/invoices'>
          <Invoices />
        </Route>
        <Route path='/return_credit_notes'>
          <Returncreditnotes />
        </Route>

        <Route path='/generate_ledger'>
          <Generateledger />
        </Route>
        <Route path='/group_summary'>
          <Groupsummary />
        </Route>
        <Route path='./profit_loss'>
          <Profitloss />
        </Route>
        <Route path='/trial_balance'>
          <Trialbalance />
        </Route>
        <Route path='/balance_sheet'>
          <Balancesheet />
        </Route>

        {/* //help section routes */}

        <Route path='/help_support'>
          <Helpsupport />
        </Route>
        <Route path='/faq'>
          <FAQ />
        </Route>
        <Route path='/keyboard_shortcuts'>
          <Keyboards />
        </Route>
        <Route path='/data_migration'>
          <Datamigration />
        </Route>
        <Route path='/user_manual'>
          <User_manual />
        </Route>

    {/* Setting Route */}
        <Route path="/organizational_profile">
          <Organizationalprofile />
        </Route>
        <Route path='/user_role'>
          <Userrole />
        </Route>
        <Route path='/opening_balance'>
          <Openingbalance />
        </Route>
        <Route path='/preference'>
          <Preference />
        </Route>
        <Route path='/reminder'>
          <Reminder />
        </Route>
        <Route path='/data_backup'>
          <Databackup />
        </Route>


        {/* profile routes */}

        <Route path='/account_setting'>
          <Accountsetting />
        </Route>
        <Route path='/change_password'>
          <Changepassword />
        </Route>

      </Router>

      <Manipulation />


    </div>        
  );
}

export default App;
